[![pipeline status](https://gitlab.com/layflags/pwa-project-template/badges/master/pipeline.svg)](https://gitlab.com/layflags/pwa-project-template/commits/master)

# My personal & opinionated project template for PWAs

This is currently my preferred project setup for creating PWAs with JS. I ❤️ it, you maybe!

Take a look: https://layflags-pwa-project-template.now.sh/

## Principles

* super easy to use, extend and change
* everything you need to create a PWA
* small build size
* good Lighthouse score
* KISS

## Used software

* build: Node.js, npm, [Parcel](https://parceljs.org/)
* linting & formatting: [ESLint](https://eslint.org/), [stylelint](https://stylelint.io/), [Prettier](https://prettier.io/)
* stylesheets: [PostCSS](https://postcss.org/), [Autoprefixer](http://autoprefixer.github.io/), [Tailwind CSS](https://tailwindcss.com/)
* unit testing: [RITEway](https://github.com/ericelliott/riteway)
* e2e testing: [Cypress](https://cypress.io)

### Part of resulting App

* 💜[Preact](https://preactjs.com/) w/o compat
* 💙[Redux Bundler](https://reduxbundler.com/)
* and others, see [package.json](package.json)

## File structure

* HTML template: `src/index.html`
* App main script: `src/index.js`
* Styles: `src/styles.css`
* Components: `src/components/`
* Bundles: `src/bundles/`
* Assets: `src/assets/` (favicons, logo, etc.)
* The rest are mostly config files

## Test

### Unit

```
$ npm test               # run once
$ npm run test:watch     # run in watch mode
$ npm run test:coverage  # generate coverage report
```

### E2E

```
$ npm run test:e2e        # run once
$ npm run test:e2e:watch  # run in watch mode
```

## Lint

Happens automatically before commit thanks to [husky](https://github.com/typicode/husky)

```
$ npm run lint      # lint it
$ npm run lint:fix  # fix it
```

For showing lint errors and auto formatting I use [ALE](https://github.com/w0rp/ale), so I never use `npm run lint:fix` directly.

## Development

```
$ npm start  # start dev server (HMR, React & Redux devtools)
```

### Customize favicons

* go to https://realfavicongenerator.net/
* select an image and configure everything to your needs
* click generate and download Favicon package
* extract contents of zip into `./src/assets/`
* fix image paths in `./src/assets/site.webmanifest` from `/android-chrome-*.png` to `./android-chrome-*.png`
* adapt meta `theme-color` and `msapplication-TileColor` in `./src/index.html`

## Build

```
$ npm run build
```

## Deployment

Will be autom. deployed with [now.sh](https://now.sh/) when pushing to `master` branch.

```
$ npm run deploy
```
