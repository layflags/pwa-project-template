describe('App', () => {
  context('all pages', () => {
    beforeEach(() => {
      cy.visit('/');
    });

    it('hides splash screen', () => {
      cy.get('#splash').should('not.be.visible');
    });

    it('renders header with headline and logo', () => {
      cy.get('header')
        .should('contain', 'My project')
        .find('#logo')
        .should('exist');
    });

    it('renders navigation', () => {
      cy.get('header nav a:first-child')
        .should('contain', 'Home')
        .and('have.attr', 'href', '/');
      cy.get('header nav a:nth-child(2)')
        .and('contain', 'About')
        .and('have.attr', 'href', '/about');
      cy.get('header nav a:nth-child(3)')
        .and('contain', 'GitLab')
        .and(
          'have.attr',
          'href',
          'https://gitlab.com/layflags/pwa-project-template'
        );
    });

    it('renders footer with legal infos and version info', () => {
      cy.get('footer')
        .should('contain', '© 2019')
        .and('contain', 'Belza Digital')
        .and('contain', 'Legal notice')
        .and('contain', 'Version');
    });
  });

  context('home page', () => {
    beforeEach(() => {
      cy.visit('/');
    });

    it('renders headline and content', () => {
      cy.get('#page')
        .should('contain', 'Hello World!!!')
        .and('contain', 'AppTime');
    });
  });

  context('about page', () => {
    beforeEach(() => {
      cy.visit('/about');
    });

    it('renders headline and content', () => {
      cy.get('#page')
        .should('contain', 'About')
        .and('contain', 'This is my PWA project template.');
    });
  });
});
