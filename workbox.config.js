module.exports = {
  globDirectory: 'build/',
  globPatterns: ['**/*.{png,xml,ico,html,svg,webmanifest,js,css}'],
  swSrc: 'src/sw.js',
  swDest: 'build/sw.js',
  dontCacheBustURLsMatching: /\.\w{8}\./,
};
