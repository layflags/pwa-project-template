/* global importScripts, workbox */
/* eslint-disable no-restricted-globals */

importScripts(
  'https://storage.googleapis.com/workbox-cdn/releases/3.2.0/workbox-sw.js'
);

// development only
if (typeof process !== 'undefined') {
  workbox.core.setCacheNameDetails({ prefix: process.env.APP_ID });
}

workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute([]);

workbox.routing.registerNavigationRoute('/', {
  blacklist: [/\/__/], // ignore cypress related URLs
});

function onMessage(event) {
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting();
  }
}

self.addEventListener('message', onMessage);
