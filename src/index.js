import 'focus-outline-manager';
import { h, render } from 'preact';
import { Provider } from 'redux-bundler-preact';

import App from './components/App';
import createStore from './bundles';

const renderApp = (store, merge) =>
  render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('app'),
    merge
  );

// hides splash screen
document.body.classList.add('app-running');

if (process.env.NODE_ENV === 'development' && module.hot && !window.Cypress) {
  // HMR setup rerenders view but keeps store
  let { store, view } = module.hot.data || {};
  store = store || createStore();
  view = renderApp(store, view);
  module.hot.dispose(data => {
    Object.assign(data, { store, view });
  });

  // eslint-disable-next-line global-require
  require('preact/debug');
} else {
  renderApp(createStore());
}
