import { h } from 'preact';
import { getNavHelper } from 'internal-nav-helper';
import { connect } from 'redux-bundler-preact';

const Main = ({ doUpdateUrl, children, ...props }) => (
  /* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
  /* eslint-disable jsx-a11y/click-events-have-key-events */
  <main onclick={getNavHelper(doUpdateUrl)} class={props.class}>
    {children}
  </main>
);

export default connect(
  'doUpdateUrl',
  Main
);
