import { h } from 'preact';
import { connect } from 'redux-bundler-preact';

import ErrorBox from './ErrorBox';

const OfflineInfo = ({ isOnline, message = 'You are offline!' }) =>
  !isOnline && <ErrorBox message={message} />;

export default connect(
  'selectIsOnline',
  OfflineInfo
);
