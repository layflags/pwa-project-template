import { h } from 'preact';
import { connect } from 'redux-bundler-preact';

import Page from '../Page';
import Headline from '../Headline';

const Home = ({ appTime }) => (
  <Page>
    <Headline>Hello World!!!</Headline>
    <p>AppTime: {appTime}</p>
  </Page>
);

export default connect(
  'selectAppTime',
  Home
);
