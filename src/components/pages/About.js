import { h } from 'preact';

import Page from '../Page';
import Headline from '../Headline';

const About = () => (
  <Page>
    <Headline>About</Headline>
    <p>This is my PWA project template.</p>
  </Page>
);

export default About;
