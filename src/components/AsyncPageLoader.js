import { h } from 'preact';
import { connect } from 'redux-bundler-preact';

import AsyncComponent from './AsyncComponent';

const AsyncPageLoader = ({
  route,
  doAsyncPageStart,
  doAsyncPageFinish,
  doAsyncPageFail,
}) => (
  <AsyncComponent
    getComponent={route}
    onStarted={doAsyncPageStart}
    onFinished={doAsyncPageFinish}
    onFailed={doAsyncPageFail}
  />
);

// TODO: create new HOC connectMapped:
//
// export default connectMapped(
//   {
//     getComponent: 'selectRoute',
//     onStarted: 'doAsyncPageStart',
//     onFinished: 'doAsyncPageFinish',
//     onFailed: 'doAsyncPageFail',
//   },
//   AsyncComponent
// );

export default connect(
  'selectRoute',
  'doAsyncPageStart',
  'doAsyncPageFinish',
  'doAsyncPageFail',
  AsyncPageLoader
);
