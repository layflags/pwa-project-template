import { h } from 'preact';
import render from 'preact-render-to-string';
import { describe } from 'riteway';

import styled from './styled';

describe('styled', async assert => {
  assert({
    given: 'a tag name',
    should: 'return a styled component',
    actual: typeof styled('h1'),
    expected: 'function',
  });

  {
    const H1 = styled('h1');
    assert({
      given: 'a styled component',
      should: 'renders markup',
      actual: render(<H1>Hi</H1>),
      expected: '<h1>Hi</h1>',
    });
  }

  {
    const H1 = styled('h1', 'text-blue text-2xl');
    assert({
      given: 'a styled component with given class names',
      should: 'renders markup',
      actual: render(<H1>Hi</H1>),
      expected: '<h1 class="text-blue text-2xl">Hi</h1>',
    });
  }

  {
    const H1 = styled('h1', 'text-blue');
    assert({
      given: 'a styled component with given class name',
      should: 'renders markup and merges class name',
      actual: render(<H1 class="text-2xl">Hi</H1>),
      expected: '<h1 class="text-blue text-2xl">Hi</h1>',
    });
  }

  {
    const H1 = styled('h1', 'text-blue');
    assert({
      given: 'a styled component',
      should: 'renders markup and passes props',
      actual: render(<H1 id="headline">Hi</H1>),
      expected: '<h1 id="headline" class="text-blue">Hi</h1>',
    });
  }

  {
    const H1 = styled('h1', 'text-blue');
    assert({
      given: 'a styled component',
      should: 'renders markup and replaces node name via prop component',
      actual: render(<H1 component="p">Hi</H1>),
      expected: '<p class="text-blue">Hi</p>',
    });
  }
});
