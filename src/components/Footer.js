import { h } from 'preact';
import cn from 'classnames';

import ExternalLink from './ExternalLink';

const { GIT_REV } = process.env;
const { BUILD_TIME } = process.env;
const BUILD_YEAR = new Date(BUILD_TIME).getFullYear();

const Footer = props => (
  <footer class={cn('text-center', props.class)}>
    <p>
      <small class="text-sm">
        &copy; {BUILD_YEAR} by{' '}
        <ExternalLink href="https://belza.digital">Belza Digital</ExternalLink>{' '}
        &bull;{' '}
        <ExternalLink href="https://belza.digital/impressum">
          Legal notice
        </ExternalLink>
      </small>
    </p>
    <p class="text-sm pt-4">
      Version: {GIT_REV} ({BUILD_TIME})
    </p>
  </footer>
);

export default Footer;
