import preact from 'preact';

class AsyncComponent extends preact.Component {
  static defaultProps = {
    onStarted: Function.prototype,
    onFinished: Function.prototype,
    onFailed: Function.prototype,
  };

  state = { Component: null };

  componentWillMount() {
    if (!this.state.Component) {
      this.updateComponent(this.props.getComponent);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.getComponent !== nextProps.getComponent) {
      this.updateComponent(nextProps.getComponent);
    }
  }

  updateComponent(getComponent) {
    this.props.onStarted();
    getComponent()
      .then(({ default: Component }) => {
        this.setState({ Component });
        this.props.onFinished();
      })
      .catch(this.props.onFailed);
  }

  render(_, { Component }) {
    return Component ? preact.h(Component) : null;
  }
}

export default AsyncComponent;
