import styled from './styled';

const Headline = styled('h2', 'mb-4');

export default Headline;
