import { h } from 'preact';
import cn from 'classnames';

const styled = (defaultComponent, className) => {
  const StyledComponent = ({ component, ...props }) =>
    h(component || defaultComponent, {
      ...props,
      class: cn(className, props.class) || null,
    });
  StyledComponent.displayName = 'styled';
  return StyledComponent;
};

export default styled;
