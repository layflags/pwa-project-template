import { h } from 'preact';
import cn from 'classnames';

const ErrorBox = ({ message, ...props }) => (
  <div class={cn('bg-red shadow px-4 py-2', props.class)}>
    <p class="text-white text-xs leading-none">{message}</p>
  </div>
);

export default ErrorBox;
