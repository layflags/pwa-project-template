import { h } from 'preact';
import cn from 'classnames';

const Page = ({ children, ...props }) => (
  <section id="page" class={cn('bg-white shadow p-4', props.class)}>
    {children}
  </section>
);

export default Page;
