import { h } from 'preact';
import { connect } from 'redux-bundler-preact';
import cn from 'classnames';

import ProgressBar from './ProgressBar';

const AsyncLoadingLine = ({ asyncActive }) => (
  <ProgressBar class={cn(asyncActive || 'hidden', 'fixed pin-t pin-l pin-r')} />
);

export default connect(
  'selectAsyncActive',
  AsyncLoadingLine
);
