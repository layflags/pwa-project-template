import { h } from 'preact';

import Main from './Main';
import Footer from './Footer';
import Header from './Header';
import Navigation from './Navigation';

const Layout = ({ children }) => (
  <Main class="m-4">
    <Header>
      <Navigation class="my-4" />
    </Header>
    {children}
    <Footer class="mt-4" />
  </Main>
);

export default Layout;
