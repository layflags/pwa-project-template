import { h } from 'preact';
import cn from 'classnames';
import { connect } from 'redux-bundler-preact';

import ExternalLink from './ExternalLink';

const isExternal = url => /^https?:\/\//.test(url);

const navItems = [
  { url: '/', label: 'Home' },
  { url: '/about', label: 'About' },
  {
    url: 'https://gitlab.com/layflags/pwa-project-template',
    label: 'GitLab',
  },
];

const Navigation = ({ pathname, ...props }) => (
  <nav class={props.class}>
    {navItems.map(({ url, label }) => {
      const Link = isExternal(url) ? ExternalLink : 'a';
      return (
        <Link
          class={cn(url === pathname ? 'underline' : 'no-underline', 'mr-2 ')}
          href={url}
          key={url}
        >
          {label}
        </Link>
      );
    })}
  </nav>
);

export default connect(
  'selectPathname',
  Navigation
);
