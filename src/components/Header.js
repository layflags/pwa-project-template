import { h } from 'preact';

import logoSvgData from '../assets/logo.svg'; // using `parcel-plugin-inlinesvg`
import SVGInline from './SVGInline';

const Header = ({ children }) => (
  <header>
    <h1 class="flex justify-between">
      My project
      <SVGInline data={logoSvgData} width="2em" height="0.75em" id="logo" />
    </h1>
    {children}
  </header>
);

export default Header;
