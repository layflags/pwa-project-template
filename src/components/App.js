import { h } from 'preact';

import AsyncLoadingLine from './AsyncLoadingLine';
import AsyncPageLoader from './AsyncPageLoader';
import Layout from './Layout';
import OfflineInfo from './OfflineInfo';
import UpdateNotifier from './UpdateNotifier';

const App = () => (
  <Layout>
    <AsyncLoadingLine />
    <AsyncPageLoader />
    <OfflineInfo />
    <UpdateNotifier />
  </Layout>
);

export default App;
