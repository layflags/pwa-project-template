import { h } from 'preact';

/* eslint-disable react/no-danger */
const SVGInline = ({ data, width, height, style, ...props }) => (
  <span
    dangerouslySetInnerHTML={{ __html: data }}
    style={{ display: 'inline-block', width, height, ...style }}
    {...props}
  />
);

export default SVGInline;
