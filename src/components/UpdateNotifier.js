import { h } from 'preact';
import { connect } from 'redux-bundler-preact';
import cn from 'classnames';

const UpdateNotifier = ({
  swUpdateAvailable,
  swUpdateDeclined,
  doUpdateSw,
  doDeclineUpdateSw,
}) => (
  <div
    class="fixed pin-l pin-r mb-4 flex justify-center"
    style={{
      bottom: -100,
      transition: 'transform 200ms',
      transform: `translateY(${
        swUpdateAvailable && !swUpdateDeclined ? -100 : 0
      }px)`,
    }}
  >
    <p
      class={cn(
        'bg-grey-darkest rounded pl-4 pr-1',
        'text-white text-sm flex items-center'
      )}
    >
      A new version is available{' '}
      <button
        type="button"
        onclick={doUpdateSw}
        class="text-blue-light uppercase font-bold p-3 pr-0"
      >
        Refresh
      </button>
      <button
        type="button"
        onclick={doDeclineUpdateSw}
        class="text-white uppercase font-bold p-3"
      >
        ✕
      </button>
    </p>
  </div>
);

export default connect(
  'selectSwUpdateAvailable',
  'selectSwUpdateDeclined',
  'doUpdateSw',
  'doDeclineUpdateSw',
  UpdateNotifier
);
