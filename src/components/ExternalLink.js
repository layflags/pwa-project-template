import { h } from 'preact';

const ExternalLink = ({ href, children, ...props }) => (
  <a href={href} {...props} target="_blank" rel="noopener noreferrer">
    {children}
  </a>
);

export default ExternalLink;
