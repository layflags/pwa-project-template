import {
  composeBundlesRaw,
  appTimeBundle,
  asyncCountBundle,
  onlineBundle,
  createUrlBundle,
  createReactorBundle,
  debugBundle,
  // createCacheBundle,
  // createAsyncResourceBundle,
  // createGeolocationBundle,
} from 'redux-bundler';

import routes from './routes';
import asyncPage from './asyncPage';
import serviceWorker from './service-worker';

const allBundles = [
  appTimeBundle,
  asyncCountBundle,
  onlineBundle,
  createUrlBundle(),
  createReactorBundle(),
  debugBundle,
  routes,
  asyncPage,
  serviceWorker,
];

const createStore = composeBundlesRaw(...allBundles);

// HMR setup always reloads the page
if (process.env.NODE_ENV === 'development' && module.hot && !window.Cypress) {
  module.hot.accept(() => {
    window.location.reload();
  });
}

export default createStore;
