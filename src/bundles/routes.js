import { createRouteBundle } from 'redux-bundler';

export default createRouteBundle({
  '/': () => import('../components/pages/Home'),
  '/about': () => import('../components/pages/About'),
});
