const STARTED = 'ASYNC_PAGE_STARTED';
const FINISHED = 'ASYNC_PAGE_FINISHED';
const FAILED = 'ASYNC_PAGE_FAILED';

export default {
  name: 'asyncPage',

  doAsyncPageStart: () => ({ type: STARTED }),
  doAsyncPageFinish: () => ({ type: FINISHED }),
  doAsyncPageFail: error => ({
    type: FAILED,
    error,
  }),

  reducer: (
    state = {
      isLoading: false,
      lastSuccess: null,
      lastError: null,
      errorType: null,
    },
    { type, error }
  ) => {
    if (type === STARTED) return { ...state, isLoading: true };
    if (type === FINISHED)
      return {
        isLoading: false,
        lastSuccess: Date.now(),
        lastError: null,
        errorType: null,
      };
    if (type === FAILED)
      return {
        ...state,
        isLoading: false,
        lastError: Date.now(),
        errorType: (error && error.message) || error,
      };
    return state;
  },
};
