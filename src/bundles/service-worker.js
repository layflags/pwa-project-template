let newWorker;

const AVAILABLE = 'SW_AVAILABLE';
const REGISTERED = 'SW_REGISTERED';
const UPDATE_AVAILABLE = 'SW_UPDATE_AVAILABLE';
const UPDATE_DECLINED = 'SW_UPDATE_DECLINED';
const UPDATING = 'SW_UPDATING';

const name = 'serviceWorker';

const initialState = {
  available: false,
  registered: false,
  updateAvailable: false,
  updateDeclined: false,
  updating: false,
};

const reducer = (state = initialState, { type }) => {
  if (type === AVAILABLE) return { ...state, available: true };
  if (type === REGISTERED) return { ...state, registered: true };
  if (type === UPDATE_AVAILABLE) return { ...state, updateAvailable: true };
  if (type === UPDATE_DECLINED) return { ...state, updateDeclined: true };
  if (type === UPDATING) return { ...state, updating: true };
  return state;
};

const selectSwAvailable = store => store[name].available;
const selectSwRegistered = store => store[name].registered;
const selectSwUpdateAvailable = store => store[name].updateAvailable;
const selectSwUpdateDeclined = store => store[name].updateDeclined;
const selectSwUpdating = store => store[name].updating;

const doUpdateSw = () => ({ store, dispatch }) => {
  if (!newWorker) return;
  if (!store.selectSwUpdateAvailable()) return;
  if (store.selectSwUpdating()) return;
  dispatch({ type: UPDATING });
  newWorker.postMessage({ action: 'skipWaiting' });
};

const doDeclineUpdateSw = () => ({ type: UPDATE_DECLINED });

const init = store => {
  if ('serviceWorker' in navigator) {
    store.dispatch({ type: AVAILABLE });
    window.addEventListener('load', () => {
      navigator.serviceWorker.register('../sw.js').then(reg => {
        store.dispatch({ type: REGISTERED });
        reg.addEventListener('updatefound', () => {
          // an updated service worker has appeared in reg.installing!
          newWorker = reg.installing;
          newWorker.addEventListener('statechange', () => {
            // skip if new service worker state is NOT `installed`
            if (newWorker.state !== 'installed') return;
            // skip if NO new service worker available
            if (!navigator.serviceWorker.controller) return;
            store.dispatch({ type: UPDATE_AVAILABLE });
          });
        });
      });
      let refreshing;
      navigator.serviceWorker.addEventListener('controllerchange', () => {
        if (refreshing) return;
        window.location.reload();
        refreshing = true;
      });
    });
  }
};

export default {
  name,
  reducer,
  init,
  doUpdateSw,
  doDeclineUpdateSw,
  selectSwAvailable,
  selectSwRegistered,
  selectSwUpdateAvailable,
  selectSwUpdateDeclined,
  selectSwUpdating,
};
